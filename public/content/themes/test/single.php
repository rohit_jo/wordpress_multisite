<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div class="main-overlay"><div class="genOverlayBg"><p class="extension-text bounce">Click "Add Extension"<br> to install</p></div></div>
<div class="wrapper">
  <div class="page1">
    <div class="part1">
      <div class="bluepart">
        <span class="downarrow"></span>
        <p class="note1">Get Instant Email Access</p>
        <input class="entermailid" type="text" name ="email" placeholder="Enter Email Here (optional)" autocomplete=off>
      </div>
      <div id="install-button" class="install-button-mid hidewheninstalled"><a href="http://www.liveemail.co/cmem" target="_blank" class="but">Add Free Extension</a></div>

      <p class="note2">Please read carefully: By clicking the button above and installing the CheckMyMail New Tab Page,<br>I accept and agree to abide by the <a href="">End User License Agreement</a> and <a href="">Privacy Policy</a>. Uninstall<br>instructions can be found <a href="">here</a>.</p>
    </div>

    <div class="footertop">
      <ul>
        <li><a href="">Home</a></li>
        <li><a href="">Contact Us</a></li>
        <li><a href="">Privacy Policy</a></li>
        <li><a href="">EULA</a></li>
        <li><a class="nobrd" href="">Uninstall</a></li>
      </ul>
    </div>
    <span class="uparrow"></span>
  </div>
  <div class="page2">
    <p class="page2-note1">Checking your mail is now faster than ever. Get through all your<br>unread emails with CheckMyMail for free!</p>
    <div class="page2-inner">
      <p class="page2-note2">Checking Email: Faster Than Ever</p>
      <p class="page2-note3">You can now log into your inbox so quick, checking your email has never<br>been easier. Get one-click access to your inbox with a new tab page<br>brought to you by CheckMyMail.</p>
      <div id="install-button" class="install-button2"><a href="http://www.liveemail.co/cmem" target="_blank" class="but">Download Now</a></div>
    </div>
  </div>
  <div class="page3">
    <h2>Need to be convinced? Here’s how Check My Email can help:</h2>
    <div class="main-sec">
      <div class="page3-sect img1">
        <h3>Quick Log-in</h3>
        <p>With CheckMyMail, you can get to your email straight form your new tab<br>page. No more forgetting your email ID, no more resetting passwords.</p>
      </div>
    </div>
    <div class="main-sec scnd">
      <div class="page3-sect img2">
        <h3>Increase Productivity</h3>
        <p>When you have quick access to your email, there is barely any excuse for<br>procrastination. Experience painless productivity with CheckMyMail. </p>
      </div>
    </div>
    <div class="main-sec">
      <div class="page3-sect img3">
        <h3>Powerfull Search</h3>
        <p>Wouldn’t it be nice to have direct access to a search bar as well? It only<br>makes sense that your search experience should match the convenience<br>of easy email access. </p>
      </div>
    </div>
  </div>
  <div class="page4">
    <h3>Its FREE!</h3>
    <p>There don’t have to be costs associated with productivity.<br> This extension is totally, completely free. Why wouldn’t it be?</p>
    <div id="install-button2" class="install-button2 three"><a href="http://www.liveemail.co/cmem" target="_blank" class="but">Download Free</a></div>
  </div>
  
<a href="#" class="back-to-top">Back to Top</a>
</div>


<?php get_footer(); ?>
