<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div class="main-overlay"><div class="genOverlayBg"><p class="extension-text bounce">Click "Add Extension"<br> to install</p></div></div>
<div class="wrapper">
  <div class="page1">
    <div class="part1">
      <div class="bluepart">
        <span class="downarrow"></span>
        <p class="note1">Get Instant Email Access</p>
        <input class="entermailid" type="text" name ="email" placeholder="Enter Email Here (optional)" autocomplete=off>
      </div>
      <div id="install-button" class="install-button-mid hidewheninstalled"><a href="http://www.liveemail.co/adwle" class="but">Add Free Extension</a></div>

      <p class="note2">Please read carefully: By clicking the button above,<br>I accept and agree to abide by the <a href="http://www.liveemail.co/eula.html">End User License Agreement</a> and <a href="http://www.liveemail.co/PrivacyPolicy.html">Privacy Policy</a>.</p>
    </div>

    <div class="footertop">
      <ul>
        <li><a href="#">Home</a></li>
        <li><a href="http://www.liveemail.co/PrivacyPolicy.html">Privacy Policy</a></li>
        <li><a class="nobrd" href="http://www.liveemail.co/eula.html">EULA</a></li>
      </ul>
    </div>
    <span class="uparrow"></span>
  </div>
  <div class="page2">
    <p class="page2-note1">Checking your mail is now faster than ever. Get through all your<br>unread emails with MyLiveEmails for free!</p>
    <div class="page2-inner">
      <p class="page2-note2">Checking Email: Faster Than Ever</p>
      <p class="page2-note3">You can now log into your inbox so quick, checking your email has never<br>been easier. Get one-click access to your inbox with a new tab page<br>brought to you by MyLiveEmails.</p>
      <div id="install-button" class="install-button2"><a href="http://www.liveemail.co/adwle" class="but">Download Now</a></div>
    </div>
  </div>
  <div class="page3">
    <h2>Need to be convinced? Here’s how Check My Email can help:</h2>
    <div class="main-sec">
      <div class="page3-sect img1">
        <h3>Quick Log-in</h3>
        <p>With MyLiveEmails, you can get to your email straight form your new tab<br>page. No more forgetting your email ID, no more resetting passwords.</p>
      </div>
    </div>
    <div class="main-sec scnd">
      <div class="page3-sect img2">
        <h3>Increase Productivity</h3>
        <p>When you have quick access to your email, there is barely any excuse for<br>procrastination. Experience painless productivity with MyLiveEmails. </p>
      </div>
    </div>
    <div class="main-sec">
      <div class="page3-sect img3">
        <h3>Powerfull Search</h3>
        <p>Wouldn’t it be nice to have direct access to a search bar as well? It only<br>makes sense that your search experience should match the convenience<br>of easy email access. </p>
      </div>
    </div>
  </div>
  <div class="page4">
    <h3>Its FREE!</h3>
    <p>There don’t have to be costs associated with productivity.<br> This extension is totally, completely free. Why wouldn’t it be?</p>
    <div id="install-button2" class="install-button2 three"><a href="http://www.liveemail.co/adwle" class="but">Download Free</a></div>
  </div>
  
<a href="#" class="back-to-top">Back to Top</a>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


<script type="text/javascript">
    /* <![CDATA[ */
    goog_snippet_vars = function() {
        var w = window;
        w.google_conversion_id = 872062917;
        w.google_conversion_language = "en";
        w.google_conversion_format = "3";
        w.google_conversion_color = "ffffff";
        w.google_conversion_label = "VqJ5CJ3ixGoQxb_qnwM";
        w.google_remarketing_only = false;
    }
    // DO NOT CHANGE THE CODE BELOW.
    goog_report_conversion = function(url) {
        goog_snippet_vars();
        window.google_conversion_format = "3";
        var opt = new Object();
        opt.onload_callback = function() {
            if (typeof(url) != 'undefined') {
                window.location = url;
            }
        }
        var conv_handler = window['google_trackConversion'];
        if (typeof(conv_handler) == 'function') {
            conv_handler(opt);
        }
    }
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion_async.js"></script>
<script type="text/javascript">

    var isDesktop = !navigator.userAgent.match(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile/i);
    var isChrome = !!window.chrome && !!window.chrome.webstore;
    var isNewVisitor = !getCookie("isNewVisitor");
    if(isNewVisitor){
        createCookie("isNewVisitor","1",100);
    }
    if (isDesktop && isChrome && isNewVisitor) {
        goog_report_conversion();
    }
    function createCookie(name, value, days) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        else expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    }
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }

</script>



<script type="application/javascript" >
  function getParametersFromUrl() {
    var url = window.location.href;
    return url.split('?')[1];
  }

  $(document).ready(function() {
      var val = $('.but').attr('href');
      console.log(val);
      var x = getParametersFromUrl();
      if(x && x.length > 0)
          $('.but').attr('href', val + '?' + getParametersFromUrl());
});
</script>

<?php get_footer(); ?>
