<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

  <section class="part1">
    <div class="cont1">
      <div class="padd">
        <h3>Get Instant Maps On The Go</h3>
        <div class="directions">
          <div class="icns"></div>
          <div class="txtbx">
            <form class="myfrm">
              <div>
                <input type="text" id="input1" placeholder="Your Location" class="inputbx inp2" required autocomplete="off" />
                <ul class="autolist tt-menu" style="display:none;"></ul>
              </div>
              <div>
                <input type="text" id="input2" placeholder="Destination" class="inputbx" required autocomplete="off" />
                <ul class="autolist tt-menu" style="display:none;"></ul>
              </div>
              <input type="submit" class="submitBtn" value="View Directions">
            </form> 
          </div>
        </div>
        <div class="btm">
          <h3 class="trvl">Travel Mode</h3>
          <div class="travelModes">
            <span class="car active"></span>
            <span class="bus"></span>
            <span class="rail"></span>
            <span class="cycle"></span>
            <span class="walk"></span>
          </div>
        </div>
        <div class="optns">
          <div class="padd2">
            <div class="topOptns">
              <h3>Route Options</h3>
              <input type="checkbox" value="Ferries" class="checkbox" name="chk1" /><label for="Ferries">Ferries</label>
            </div>
            <div class="btmOptns">
              <h3>Distance</h3>
              <input type="radio" class="radiobtns" value="Automatic" name="rd1" checked /><label for="Automatic">Automatic</label>
              <input type="radio" class="radiobtns" value="Kms" name="rd1"><label for="Kms">Kms</label>
              <input type="radio" class="radiobtns" value="Miles" name="rd1"><label for="Miles">Miles</label>
            </div>
          </div>  
        </div>
      </div>  
    </div>
    <div class="cont2">
      <div class="padd">
        <h3>Fastest Route Found</h3>
        <p>Install My Map Guide Extension<br>to get quick access to your maps for FREE</p>
        <a class="submitBtn3 but" target="_blank" href="http://www.checkmaps.co/admap">Install Now</a>
      </div>
    </div>
</section>
<section class="part2">
    <span class="arrow"></span>
    <div class="content">
      <div class="hdr">Welcome to <span>My Map Guide</span></div>
      <p>Reaching your destination is now simpler than ever</p>
      <div class="secImg"></div>
      <p>Go to your preferred maps straight from your<br>new tab page with <span>‘My Map Guide’</span> for FREE</p>
      <div class="sepr"></div>
      <div class="innerContent">
        <ul>
          <li>
            <div class="indiImg">
              <span class="img img1"></span>
              <span class="imgCt">Quick Mapping</span>
            </div>
            <div class="indiCont">Access maps straight form your new tab page.<br>Mapping your route doesn’t have to cut into your travel time.</div>
          </li>
          <li>
            <div class="indiImg">
              <span class="img img2"></span>
              <span class="imgCt">Credible Directions</span>
            </div>
            <div class="indiCont">People make mistakes, maps don’t!<br>Get to your destination the right way.</div>
          </li>
          <li>
            <div class="indiImg">
              <span class="img img3"></span>
              <span class="imgCt">Powerful Searches</span>
            </div>
            <div class="indiCont">Get immediate access to a great search bar.<br>This ensures that your browser gets a comprehensive upgrade.</div>
          </li>
        </ul> 
      </div>
      <div class="sepr"></div>
      <div class="innerContent2">
        <h3 class="freeCnt">It’s FREE!</h3>
        <p>This extension is totally, completely free. Why wouldn’t it be?</p>
        <a class="btnSubmt but" value="Ready to Route" target="_blank" href="http://www.checkmaps.co/admap">Ready to Route</a>
        <div class="footLinks">
          <ul>
            <li><a href="#">Home</a></li>
<!--            <li><a href="javascript:void(0);" target="_blank">Contact Us</a></li> -->
            <li><a href="http://www.checkmaps.co/PrivacyPolicy.html" target="_blank">Privacy Policy</a></li>
            <li><a href="http://www.checkmaps.co/eula.html" target="_blank">EULA</a></li>
<!--            <li><a href="javascript:void(0);" target="_blank">Uninstall</a></li> -->
          </ul>
        </div>
      </div>
    </div>
</section>

<script type="text/javascript">
    /* <![CDATA[ */
    goog_snippet_vars = function() {
        var w = window;
        w.google_conversion_id = 872078794;
        w.google_conversion_language = "en";
        w.google_conversion_format = "3";
        w.google_conversion_color = "ffffff";
        w.google_conversion_label = "pORsCLbt2GoQyrvrnwM";
        w.google_remarketing_only = false;
    }
    // DO NOT CHANGE THE CODE BELOW.
    goog_report_conversion = function(url) {
        goog_snippet_vars();
        window.google_conversion_format = "3";
        var opt = new Object();
        opt.onload_callback = function() {
            if (typeof(url) != 'undefined') {
                window.location = url;
            }
        }
        var conv_handler = window['google_trackConversion'];
        if (typeof(conv_handler) == 'function') {
            conv_handler(opt);
        }
    }
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion_async.js"></script>
<script type="text/javascript">

    var isDesktop = !navigator.userAgent.match(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile/i);
    var isChrome = !!window.chrome && !!window.chrome.webstore;
    var isNewVisitor = !getCookie("isNewVisitor");
    if(isNewVisitor){
        createCookie("isNewVisitor","1",100);
    }
    if (isDesktop && isChrome && isNewVisitor) {
        goog_report_conversion();
    }
    function createCookie(name, value, days) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        else expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    }
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }

</script>

<script type="text/javascript">
var googApi = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?';
var apiKey = 'AIzaSyAQTEzGNznAjIZHPHnw9ZFchJjotqr2364';

  function getParametersFromUrl() {
      var url = window.location.href;
      return url.split('?')[1];
  }

  function loadAC(query, el){
    var urlx = googApi + 'input=' + query + '&types=geocode&key=' + apiKey;
    $.ajax({
       url: urlx,
       dataType: 'json',
       success: function(data) {
          if(data && data.status == 'OK'){
            var htm = '';
            for(var i = 0; i < data.predictions.length; i++){
              htm += '<li class="autolistli tt-suggestion">' + data.predictions[i].description + '</li>';  
            }
            $(el).siblings('ul').html(htm).show();

            $('.tt-suggestion').on('click', function(e){
              e.stopPropagation();
              var val = $(this).html();
              $(this).parents('ul').siblings('input').val(val);
              $(this).parents('ul').hide();
            });
          }
       },
       error: function(e) {
          console.log(e);
       }
    });
  }

  $(document).ready(function(){

    $('.inputbx').on('keyup', function(e){
        e.stopPropagation();
        var query = $(this).val();
        if(query.length > 3){ //minimum 3 characters
          loadAC(query, $(this));
        }
    });

    $('body').on('click', function(e){
      $('.autolist').hide();
    });

    var val = $('.but').attr('href');
        console.log(val);
        var x = getParametersFromUrl();
        
        if(x && x.length > 0) $('.but').attr('href', val + '?' + getParametersFromUrl());

    $('.travelModes').find('span').on('click', function(e){
      e.stopPropagation();
      $(this).toggleClass('active').siblings().removeClass('active');
    });


    $('.submitBtn').on('click', function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.cont1').fadeOut(100);
      $('.cont2').fadeIn(100);
    });

    $('.arrow').on('click', function(e){
      e.stopPropagation();
      $('html, body').animate({
            scrollTop: $(".part2").offset().top
        }, 500);
    });
  });
</script>

<?php get_footer(); ?>
